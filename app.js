const app = Vue.createApp({
    data() {
        return {
            courseGoalA: 'Finish the course and learn Vue!',
            courseGoalB: 'Master Vue and build amazing apps!',
            vueLink: 'https://vuejs.org/'
        };
    },
    methods: {
        outputGoal() {
            const randomNumber = Math.random();
            if (randomNumber < 0.5) {
                return this.courseGoalA;
            } else {
                return this.courseGoalB;
            }
        }
    } 
});
app.mount('#user-goal');

const appEvent = Vue.createApp({
    data() {
        return {
            counter: 0,
            name: '',
            confirmedName: ''
        };
    },
    computed: {
        fullname() {
            if (this.name === '') {
                return '';
            }
            return this.name + ' ' + 'Manzano';  
        }
    },
    methods: {
        confirmInput() {
            this.confirmedName = this.name;
        },
        setName(event) {
            this.name = event.target.value;
        },
        resetInput() {
            this.name = '';
        },
        add(num) {
            this.counter = this.counter + num;
        },
        reduce(num) {
            this.counter = this.counter - num;
        },
        submitForm() {
            alert('Submitted!');
        }
    }
});
appEvent.mount('#events');